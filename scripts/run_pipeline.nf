#!/usr/bin/env nextflow

/////
/////
///// Pipeline for analyzing the barcodes, chop 'em and cluster 'em and
///// align 'em and make 'em in a stew
///// 
/////

// Making output directories
file("./tmp").mkdirs()
file("./reports").mkdirs()

//
//
// Reading in the fastqs
//
//

Channel.fromPath('data/fastqs/*')
    .map{ 
        (file, lane, read) = (it =~ /.*_(L\d{3})_(R\d)_.*/)[0]
        [ lane, read, it ] 
        }
    .into{ input_fastq; qc_fastq }


//process do_qc {
//    publishDir 'tmp'
//// FASTQC, counts, ?
//}

process do_fastqc {
    publishDir 'tmp'
    input:
        set val(lane), val(read), 
            file(fastq) from qc_fastq
    output:
        file("*_qc") into input_fastqc
    shell: 
        '''
        mkdir !{fastq}_qc
        fastqc --threads !{task.cpus} --format fastq !{fastq} \
            -o !{fastq}_qc
        '''
}

// >amplicon
// TTAATATGGACTAAAGGAGGCTTTTGTCGACGGATCCGATATCGGTACC
// NNNNNAANNNNNTTNNNNNTTNNNNN
// ATAACTTCGTATAATGTATGCTA
// TACGAAGTTATTGCGCGGTGATCACTTATGGTACCGTTCGTATAAAGTA
// TCCTATACAAACGGTATGCGCG
// GTGATCACTTATGGTACCGTTCGTATAATGTGTACTATACGAACGGTAA
// NNNNNAANNNNNAANNNNNTTNNNNN
// GGTACCGATATCAGATCTAAGCTTGAATTCGA

process slapchop_for_nextera {
    publishDir 'tmp'
    input:
        set val(lane), val(read), 
            file(file) from input_fastq
    output:
        set val(lane), val(read), val("fail"),
            file("${read}_filter_fail.fastq") into filtered_fastq_fail
        set val(lane), val(read), val("pass"),
            file("${read}_filter_pass.fastq") into filtered_fastq_pass
    shell: 
        '''
        python3 /slapchop.py !{file} !{read}_filter \
            --bite-size 10000 --processes !{task.cpus} \
            -o "FindNexteraAdapters: input > (?<nextera>CTGTCTCTTATACACATCT){e<=1}"\
            --output-id "input.id+' '+input.description" \
            --output-seq "input" \
            --filter "nextera" \
            -v -j 0.1
        '''
}

filtered_fastq_fail
    .mix(filtered_fastq_pass)
    .branch{
            read1: it[1] == "R1"
            read2: it[1] == "R2"
        }
    .set{ filtered_fastq_branched }

// R1
// start    TTAATATGGACTAA
// precode  CCGATATCGGTACC
// postcode ATAACTTCGTATAA

process slapchop_r1_to_find_barcodes {
    publishDir 'tmp'
    input:
        set val(lane), val(read), val(pass),
            file("in_r1.fastq") from filtered_fastq_branched.read1
    output:
        set val(lane), val(read), val(pass),
            file("${read}_filter_${pass}_barcode_pass.fastq") into chopped_read1
    shell: 
        '''
        python3 /slapchop.py in_r1.fastq !{read}_filter_!{pass}_barcode \
            --bite-size 1000 --processes !{task.cpus} \
            -o "ExtractUMIsample: input > ^(?<umi>[ATCGN]{7,9})(?<sample>[ATCGN]{5,7})(TTAATATGGACTAA){e<=3}(?<rest>.*)$"\
            -o "ExtractStrain: rest > (CCGATATCGGTACC){e<=2}(?<barcode>[ATCGN]{24,28})(ATAACTTCGTATAA){e<=2}" \
            --output-id "input.id+'_'+sample.seq+'_'+umi.seq+' '+input.description" \
            --output-seq "barcode" \
            -v -j 0.1
        '''
}

//            --filter "statistics.mean(barcode.letter_annotations['phred_quality']) >= 10" \
//            --filter "statistics.mean(sample.letter_annotations['phred_quality']) >= 10" \

// TCGAATTCAAGCTTAGATCTGATATCGGTACCNNNNNAANNNNNTTNNNNNTTNNNNNTTACCGTTCGTATAGTACACATTATACGAACGGTACCATAAGTGATCAC

// R2
// TCGAATTCAAGCTT
// CTGATATCGGTACC
// TTACCGTTCGTATA


process slapchop_r2_to_find_barcodes {
    publishDir 'tmp'
    input:
        set val(lane), val(read), val(pass),
            file("in_r2.fastq") from filtered_fastq_branched.read2
    output:
        set val(lane), val(read), val(pass),
            file("${read}_filter_${pass}_barcode_pass.fastq") into chopped_read2
    shell: 
        '''
        python3 /slapchop.py in_r2.fastq !{read}_filter_!{pass}_barcode \
            --bite-size 1000 --processes !{task.cpus} \
            -o "ExtractUMIsample: input > ^(?<umi>[ATCGN]{7,9})(?<sample>[ATCGN]{5,7})(TCGAATTCAAGCTT){e<=3}(?<rest>.*)$"\
            -o "ExtractStrain: rest > (CTGATATCGGTACC){e<=2}(?<barcode>[ATCGN]{24,28})(TTACCGTTCGTATA){e<=2}" \
            --output-id "input.id+'_'+sample.seq+'_'+umi.seq+' '+input.description" \
            --output-seq "barcode" \
            -v -j 0.1
        '''
}

//process one_line_and_sort_and_join {
//    executor 'local'
//    container 'shub://darachm/singularity_runningJobs:v0.4.0'
//    input:
//        file all_fastqs
//    output:
//        file("sorted.oneline") into joined_sorted
//    shell:
//        '''
//        cat *R1.fastq | paste - - - - | sed 's/_/\t/g' \
//            | sort -k1 -S 8G \
//            > R1_sorted.oneline
//        cat *R2.fastq | paste - - - - | sed 's/_/\t/g' \
//            | sort -k1 -S 8G \
//            > R2_sorted.oneline
//        join --check-order -j 1 R1_sorted.oneline R2_sorted.oneline > sorted.oneline
//        '''
//}

//process merge_and_demux_codes {
//    publishDir 'tmp'
//    executor 'local'
//    container 'shub://darachm/singularity_runningJobs:v0.4.0'
//    input:
//        file("sorted.oneline") from joined_sorted
//        set val(pool), val(r1index), val(r2index) from primer_scheme_1
//    output:
//        set file("demuxed_r1_*"), file("demuxed_r2_*") into demuxed_paired_fastq
//    shell:
//        '''
//            echo "Doing !{pool} !{r1index} !{r2index}"
//            cat sorted.oneline \
//                | gawk '{ if ($2=="!{r1index}" && $8=="!{r2index}") {print $0}}' \
//                | gawk -F' ' '{print $1"_"$2"_"$3"_"$8"_"$9"\\n"$5"\\n+\\n"$7}' \
//                > demuxed_r1_!{pool}.fastq ;
//            cat sorted.oneline \
//                | gawk '{ if ($2=="!{r1index}" && $8=="!{r2index}") {print $0}}' \
//                | gawk -F' ' '{print $1"_"$2"_"$3"_"$8"_"$9"\\n"$11"\\n+\\n"$13}' \
//                > demuxed_r2_!{pool}.fastq ;
//        '''
//}

////////
////////
//////// Prepare fasta for bwa identifying what should be in that well
////////
////////
//process generate_reference_fasta {
//    executor 'local'
//    container 'shub://darachm/singularity_runningJobs:v0.4.0'
//    input:
//        set val(MAT), val(up4), val(down4), file(map) from plate_maps
//    output:
//        file("known_code_fasta_${MAT}") into known_code_fasta
//    shell:
//        '''
//        cat !{map} | cut -d, -f3,4,6 \
//            | gawk -F',' '{ print ">"$1"_"$2"\\n!{up4}"$3"!{down4}" }' \
//            > known_code_fasta_!{MAT}
//        '''
//}
//known_code_fasta.collect().into{ known_codes_collected; }
//process make_bwa_index {
//    executor 'local'
//    container 'shub://darachm/singularity_bwa:v0.0.2'
//    input:
//        file known_codes_collected
//    output:
//        file("known_code_fasta") into kci_fasta
//        file("known_code_fasta.amb") into kci_amb
//        file("known_code_fasta.ann") into kci_ann
//        file("known_code_fasta.bwt") into kci_bwt
//        file("known_code_fasta.pac") into kci_pac
//        file("known_code_fasta.sa") into kci_sa
//    shell:
//        '''
//        cat known_code_fasta_* > known_code_fasta
//        bwa index known_code_fasta
//        '''
//}

//process join_and_format_files_for_bwa {
//    executor 'local'
//    container 'shub://darachm/singularity_runningJobs:v0.4.0'
//    input:
//        set val(pool), file(a), file(b) from clustered_codes_paired
//    output:
//        set val(pool), file("r1_fasta"), 
//            file("r2_fasta") into clustered_codes_formatted
//    shell:
//        '''
//        join *_r1_* *_r2_* -t, -j 2 \
//            | sed 's/^@//' \
//            > joined_clustered_codes
//        cat joined_clustered_codes \
//            | gawk -F, '{ print ">" $1 "_!{pool}\\n" $2 }' \
//            > r1_fasta
//        cat joined_clustered_codes \
//            | gawk -F, '{ print ">" $1 "_!{pool}\\n" $4 }' \
//            > r2_fasta
//        '''
//}

//process align_to_known {
//    executor 'local'
//    container 'shub://darachm/singularity_bwa:v0.0.2'
//    cpus 16
//    memory 45
//    input:
//        each file("known_code_fasta") from kci_fasta
//        each file("known_code_fasta.amb") from kci_amb
//        each file("known_code_fasta.ann") from kci_ann
//        each file("known_code_fasta.bwt") from kci_bwt
//        each file("known_code_fasta.pac") from kci_pac
//        each file("known_code_fasta.sa") from kci_sa
//        set val(pool), 
//            file("r1_fasta"), file("r2_fasta") from clustered_codes_formatted
//    output:
//        set val(pool), file("${pool}_aligned_r1.sam"), 
//            file("${pool}_aligned_r2.sam") into sam_codes
//    shell:
//        '''
//        bwa mem -t !{task.cpus} -M -k 5 -w 3 -B 1 -O 1 -T 24 \
//            known_code_fasta r1_fasta \
//            > !{pool}_aligned_r1.sam
//        bwa mem -t !{task.cpus} -M -k 5 -w 3 -B 1 -O 1 -T 24 \
//            known_code_fasta r2_fasta \
//            > !{pool}_aligned_r2.sam
//#
//        '''
//}

///////
///////
/////// Tabulate observations for each pool
///////
///////

////process tabulate_observations_per_pool {
////    publishDir 'tmp'
////    executor 'local'
////    container 'shub://darachm/singularity_runningJobs:v0.4.0'
////    input:
////        set val(pool), file(sam) from sam_codes
////    output:
////        set val(pool), 
////            file("observations_tabulated_${pool}.csv") into tabulated_observations
////    shell:
////        '''
////        cat !{sam} \
////            | grep -v "SN:.*LN:" \
////            | grep -v "ID:bwa" \
////            | gawk '{print "!{pool},"$2","$3","$7","$10 }' \
////            | sort -T ./ \
////            | uniq -c \
////            | gawk '{print $1","$2}' \
////            > observations_tabulated_!{pool}.csv
////        '''
////}

